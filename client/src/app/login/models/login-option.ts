import { VcsServicesEnum } from '../../core';

export interface ILoginOption {
  name: string;
  value: VcsServicesEnum,
  available: boolean,
  icon: string;
}
