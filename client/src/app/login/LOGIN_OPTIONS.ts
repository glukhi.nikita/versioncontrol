import {VcsServicesEnum} from '../core';
import {ILoginOption} from './models';

export const LOGIN_OPTIONS: ILoginOption[] = [
  {
    name: 'GitHub',
    value: VcsServicesEnum.github,
    available: true,
    icon: '',
  },
  {
    name: 'GitLab',
    value: VcsServicesEnum.gitlab,
    available: false,
    icon: '',
  }
];
