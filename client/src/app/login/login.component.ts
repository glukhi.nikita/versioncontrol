import { Component, OnInit } from '@angular/core';

import { LOGIN_OPTIONS } from './LOGIN_OPTIONS';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  options = LOGIN_OPTIONS;

  constructor() { }

  ngOnInit(): void {
  }

}
