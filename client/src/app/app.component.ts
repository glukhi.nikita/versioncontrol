import { Component } from '@angular/core';

@Component({
  selector: 'versions-control-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';

  constructor() {}
}
