import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing-module';
import { GuestLayoutComponent, AuthenticatedLayoutComponent } from './layouts';

@NgModule({
  declarations: [
    AppComponent,
    GuestLayoutComponent,
    AuthenticatedLayoutComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
