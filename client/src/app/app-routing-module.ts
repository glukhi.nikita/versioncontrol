import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticatedLayoutComponent, GuestLayoutComponent } from './layouts';

const routes: Routes = [
  {
    path: '',
    component: AuthenticatedLayoutComponent,
  },
  {
    path: '',
    component: GuestLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: () => import('./login/login.module')
          .then(module => module.LoginModule),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
