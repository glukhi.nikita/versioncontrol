﻿using API.Data;
using API.Models;
using API.Models.Data;
using API.Repos;

namespace API.Services
{
    public class BranchesService
    {
        private readonly BranchesRepo BrachesRepository;
        private readonly UserRepo UserRepositoiry;
        private readonly UserBranchesRepo UserBranchesRepository;
        private readonly IConfiguration Configuration;

        public BranchesService(
            BranchesRepo branchesRepository,
            UserRepo userRepository,
            UserBranchesRepo userBranchesRepository,
            IConfiguration configuration
        )
        {
            BrachesRepository = branchesRepository;
            UserRepositoiry = userRepository;
            UserBranchesRepository = userBranchesRepository;
            Configuration = configuration;
        }

        public void AddBranches(string userNodeId, Branches data)
        {
            using (var context = new DataContext(Configuration))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        UserEntity user = UserRepositoiry.GetByNodeId(userNodeId);

                        if (user == null)
                        {
                            throw new ArgumentNullException("User ID not found");
                        }

                        BranchesEntity entity = ModelToEntity(data);
                        BrachesRepository.AddBranches(entity);
                        UserBranchesRepository.AddUserBranches(user, entity);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Dispose();
                        throw new Exception(ex.Message, ex);
                    }
                }
            }
        }

        private BranchesEntity ModelToEntity(Branches branches)
        {
            return new BranchesEntity
            {
                MainBranch = branches.MainBranch,
                CompareBranch = branches.CompareBranch,
                MainBranchAliases = branches.MainBranchAliases,
                CompareBranchAliases = branches.CompareBranchAliases,
                IsDefault = branches.IsDefault,
            };
        }
    }
}
