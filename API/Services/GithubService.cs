﻿using API.Models.Github;
using Microsoft.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace API.Services
{
    public class GithubService
    {
        private readonly HttpClient _httpClient;

        public GithubService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(System.Environment.GetEnvironmentVariable("GITHUB_API"));
            _httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/vnd.github.v3+json");
            _httpClient.DefaultRequestHeaders.Add(HeaderNames.UserAgent, "1.0");
        }

        public async Task<GithubUser> GetUserByUserName(string username, string token)
        {
            StringBuilder userApiUrl = new StringBuilder("/users/");
            userApiUrl.Append(username);

            StringBuilder tokenSb = new StringBuilder("token ");
            tokenSb.Append(token);

            _httpClient.DefaultRequestHeaders.Add(HeaderNames.Authorization, token.ToString());

            return await _httpClient.GetFromJsonAsync<GithubUser>(userApiUrl.ToString());
        }

        public async Task<GithubRepository[]> GetUserRepositories(string username, string token)
        {
            StringBuilder repositoriesApiUrl = new StringBuilder("/users/");
            repositoriesApiUrl.Append(username);
            repositoriesApiUrl.Append("/repos");

            StringBuilder tokenSb = new StringBuilder("token ");
            tokenSb.Append(token);

            _httpClient.DefaultRequestHeaders.Add(HeaderNames.Authorization, token.ToString());

            var repositories = await _httpClient.GetFromJsonAsync<GithubRepository[]>(repositoriesApiUrl.ToString());

            for (int i = 0; i < repositories.Length; i++)
            {
                StringBuilder rawRequest = new StringBuilder("/repos/");
                rawRequest.Append(repositories[i].full_name);
                rawRequest.Append("/contents/");
                rawRequest.Append("package.json");

                EncodedRepositoryContnent packageJsonContent = await _httpClient.GetFromJsonAsync<EncodedRepositoryContnent>(rawRequest.ToString());
                var bytes = Convert.FromBase64String(packageJsonContent.content);
                string decodedString = Encoding.UTF8.GetString(bytes);

                dynamic stuff = JsonConvert.DeserializeObject<dynamic>(decodedString);
                dynamic dependenciesJson = stuff["dependencies"];
                dynamic devDependenciesJson = stuff["devDependencies"];

                if (dependenciesJson != null)
                {
                    string dependencies = dependenciesJson.ToString();
                    Console.WriteLine(dependencies);
                }

                if (devDependenciesJson != null)
                {
                    string devDependencies = devDependenciesJson.ToString();
                    Console.WriteLine(devDependencies);
                }
            }

            GithubRepository[] result = new GithubRepository[1];

            return result;
        }
    }
}
