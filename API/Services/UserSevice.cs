﻿using API.Repos;
using API.Models;
using API.Models.Data;
using System.Text;

namespace API.Services
{
    public class UserSevice
    {
        private readonly UserRepo Repository;

        public UserSevice(UserRepo repository)
        {
            Repository = repository;
        }

        public void AddUser(User user)
        {
            try
            {
                UserEntity entity = ModelToEntity(user);
                Repository.Add(entity);
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public User GetUser(string nodeId)
        {
            try
            {
                UserEntity entity = Repository.GetByNodeId(nodeId);
                return EntityToModel(entity);
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private User EntityToModel(UserEntity entity)
        {
            User result = new User {
                NodeId = entity.NodeId,
                Name = entity.Name,
                Username = entity.Username,
                Email = entity.Email,
                VcsService = entity.VcsService,
                Packages = entity.Packages
            };

            return result;
        }

        private UserEntity ModelToEntity(User user)
        {
            UserEntity entity = new UserEntity
            { 
                NodeId = user.NodeId,
                Name = user.Name,
                Username = user.Username,
                Email = user.Email,
                VcsService = user.VcsService,
                Packages = user.Packages
            };

            return entity;
        }
    }
}
