﻿using Microsoft.EntityFrameworkCore;
using API.Models.Data;
using API.Data;

namespace API.Repos
{
    public class BranchesRepo : IDisposable
    {
        private readonly DbSet<BranchesEntity> _table;
        private readonly DataContext _db;

        public BranchesRepo(IConfiguration configuration)
        {
            _db = new DataContext(configuration);
            _table = _db.Set<BranchesEntity>();
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        internal int SaveChanges()
        {
            try
            {
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string AddBranches(BranchesEntity entity)
        {
            _db.Add(entity);
            SaveChanges();

            return entity.Id.ToString();
        }
    }
}
