﻿using Microsoft.EntityFrameworkCore;

using API.Data;
using API.Models.Data;

namespace API.Repos
{
    public class UserRepo : IDisposable
    {
        private readonly DbSet<UserEntity> _table;
        private readonly DataContext _db;

        public UserRepo(IConfiguration configuration)
        {
            _db = new DataContext(configuration);
            _table = _db.Set<UserEntity>();
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        internal int SaveChanges()
        {
            try
            {
                return _db.SaveChanges();
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public UserEntity GetOne(string id) {
            Guid guid = Guid.Parse(id);

            return _table.Find(guid);
        }

        public List<UserEntity> GetAll() => _table.ToList();

        public string GetUserIdByNodeId(string nodeId)
        {
            try
            {
                return _table.Where(user => user.NodeId == nodeId).Select(user => user.Id).Single().ToString();
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public UserEntity GetByNodeId(string nodeId)
        {
            try
            {
                return _table.Where(user => user.NodeId == nodeId).Select(user => user).First();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Add(UserEntity entity)
        {
            _table.Add(entity);
            return SaveChanges();
        }

        public int Delete(string id)
        {
            UserEntity entity = GetOne(id);
            _db.Entry(entity).State = EntityState.Deleted;
            return SaveChanges();
        }
    }
}
