﻿using Microsoft.EntityFrameworkCore;
using API.Models.Data;
using API.Data;

namespace API.Repos
{
    public class UserBranchesRepo : IDisposable
    {
        private readonly DbSet<UserBranchesEntity> _table;
        private readonly DataContext _db;

        public UserBranchesRepo(IConfiguration configuration)
        {
            _db = new DataContext(configuration);
            _table = _db.Set<UserBranchesEntity>();
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        internal int SaveChanges()
        {
            try
            {
                return _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int AddUserBranches(UserEntity user, BranchesEntity branches)
        {
            try
            {
                // _db.Attach(user);
                // _db.Attach(branches);
                UserBranchesEntity entity = new UserBranchesEntity
                {
                    User = user,
                    Branch = branches
                };

                _table.Add(entity);
                return SaveChanges();
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
