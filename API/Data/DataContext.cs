﻿using Microsoft.EntityFrameworkCore;

using API.Models.Data;

namespace API.Data
{
    public class DataContext : DbContext
    {
        private readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
        }

        DbSet<UserEntity> user { get; set; }
        DbSet<BranchesEntity> branches { get; set; }
        DbSet<Repositories> repositories { get; set; }
        DbSet<UserBranchesEntity> userBranches { get; set; }
        DbSet<UserRepository> userRepositories { get; set; }
    }
}
