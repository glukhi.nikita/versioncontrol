using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Repos;
using API.Services;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var services = builder.Services;

// Add services to the container.
services.AddDbContext<DataContext>(options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

services.AddTransient<UserRepo>();
services.AddTransient<BranchesRepo>();
services.AddTransient<UserBranchesRepo>();

services.AddScoped<UserSevice>();
services.AddScoped<BranchesService>();

services.AddControllers();
services.AddSwaggerGen();

services.AddHttpClient<GithubService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
