﻿namespace API.Models
{
    public class Branches
    {
        public string MainBranch { get; set; }
        public string CompareBranch { get; set; }
        public string MainBranchAliases { get; set; }
        public string CompareBranchAliases { get; set; }
        public bool IsDefault { get; set; }
    }
}
