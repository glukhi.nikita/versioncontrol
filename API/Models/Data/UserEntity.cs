﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Models.Enums;

namespace API.Models.Data
{
    public class UserEntity : EntityBase
    {
        [Required, Column("nodeId")]
        public string NodeId { get; set; }
        [Required, MaxLength(50), Column("name")]
        public string Name { get; set; }
        [Required, MaxLength(50), Column("username")]
        public string Username { get; set; }
        [Required, MaxLength(100), Column("email")]
        public string Email { get; set; }
        [Required, Column("vcsService")]
        public ServicesEnum VcsService { get; set; }
        [MaxLength(1000), Column("packages")]
        public string Packages { get; set; }
        public virtual UserBranchesEntity UserBranch { get; set; }
        public virtual UserRepository Repository { get; set; }
    }
}
