﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace API.Models.Data
{
    public class UserBranchesEntity : EntityBase
    {
        [Required, ForeignKey("userId")]
        public virtual UserEntity User { get; set; }
        [Required, ForeignKey("branchId")]
        public virtual BranchesEntity Branch { get; set; }
    }
}
