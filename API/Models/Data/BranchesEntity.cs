﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Data
{
    public class BranchesEntity : EntityBase
    {
        [Required, MaxLength(250), Column("mainBranch")]
        public string MainBranch { get; set; }
        [Required, MaxLength(250), Column("compareBranch")]
        public string CompareBranch { get; set; }
        [Required, MaxLength(250), Column("mainBranchAliases")]
        public string MainBranchAliases { get; set; }
        [Required, MaxLength(250), Column("compareBranchAliases")]
        public string CompareBranchAliases { get; set; }
        [Required, Column("isDefault")]
        public bool IsDefault { get; set; }
        public virtual UserBranchesEntity UserBranch { get; set; }
    }
}
