﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace API.Models.Data
{
    public class UserRepository : EntityBase
    {
        [Required, ForeignKey("userId")]
        public virtual UserEntity User { get; set; }
        [Required, ForeignKey("repositoryId")]
        public virtual Repositories Repository { get; set; }
    }
}
