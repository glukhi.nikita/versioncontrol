﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Data
{
    public class Repositories : EntityBase
    {
        [Required, Column("nodeId")]
        public string NodeId { get; set; }
        [Required, MaxLength(250), Column("name")]
        public string Name { get; set; }
        [Required, MaxLength(1000), Column("packages")]
        public string Packages { get; set; }
        public virtual UserRepository UserRepository { get; set; }
    }
}
