﻿namespace API.Models.Github
{
    public class GithubRepository
    {
        public int id { get; set; }
        public string node_id { get; set; }
        public string name { get; set; }
        public string full_name { get; set; }
        public bool isPrivate { get; set; }
    }
}
