﻿using API.Models.Enums;

namespace API.Models
{
    public class User
    {
        public string NodeId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public ServicesEnum VcsService { get; set; }
        public string Packages { get; set; }
    }
}
