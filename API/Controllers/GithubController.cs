﻿using API.Models;
using API.Models.Github;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("/api/github")]
    public class GithubController : ControllerBase
    {

        private readonly GithubService _githubService;
        public GithubUser user { get; set; }

        public GithubController(GithubService githubService)
        {
            _githubService = githubService;
        }

        [HttpGet("user/{username}")]
        public async Task GetUserName(string username)
        {
            try
            {
                user = await _githubService.GetUserByUserName(username, System.Environment.GetEnvironmentVariable("GITHUB_TOKEN"));

                Console.WriteLine(user.company);
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [HttpGet("user/{username}/repositories")]
        public async Task GetUserRepositories(string username)
        {
            try 
            {
                await _githubService.GetUserRepositories(username, System.Environment.GetEnvironmentVariable("GITHUB_TOKEN"));
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [HttpPut("user/{id}/repositories")]
        public async Task UpdateUserRepositories(string id)
        {
            try
            {
                await _githubService.GetUserRepositories("NikitaGlukhi", System.Environment.GetEnvironmentVariable("GITHUB_TOKEN"));
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
