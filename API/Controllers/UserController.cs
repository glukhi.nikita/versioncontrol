﻿using Microsoft.AspNetCore.Mvc;
using API.Models;
using API.Services;

namespace API.Controllers
{
    [ApiController]
    [Route("/api/user")]
    public class UserController : ControllerBase
    {
        private readonly UserSevice _userSevice;
        private readonly BranchesService _branchesService;

        public UserController(UserSevice userService, BranchesService branchesService)
        {
            _userSevice = userService;
            _branchesService = branchesService;
        }

        [HttpPost]
        public void AddUser(User user)
        {
            try 
            {
                _userSevice.AddUser(user);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("{nodeId}")]
        public User GetUSer(string nodeId)
        {
            try
            {
                return _userSevice.GetUser(nodeId);
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost("{userNodeId}/branches")]
        public void AddUserBranches(string userNodeId, Branches data)
        {
            try
            {
                _branchesService.AddBranches(userNodeId, data);
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
